try {
  AudioContext = window.AudioContext || window.webkitAudioContext;
  var context = new AudioContext();
}
catch (e) {
  alert("Sorry, it doesn't look like your browser supports the Web Audio API.  Try Chrome.");
}

//cancel right-click menu as it swallows mouseup events.
window.oncontextmenu = function(e) {
  e.preventDefault();
  e.stopPropagation();
};

function shouldPlay() {
}

function getTableElements(table) {
 /*
MMMMMMMMWWWWWWWWWMMMMBBMBBVVVMRRBW####W#WBRBMWWWWW#WMMWWWMBBBBMMWWWWMMWWWWWWWWWM
MMMMMMMMWWWWWWWWMMMMBBBBBBYRBBMBMW###WMMBRRRBMMMW####WWWWWWMBBMMWWWWMMWWWMWMMWWM
MMMMMMMMWWWWWWWWMMMMMBBBItRBBMWWW###WXVIIYVRRBBMMW#WWWWWW###WBBMWWWWMMWWWMWMWWWM
BMMMMMMMWWWWWWWWMMMBBBBBBBMBRRWMBWMBVi==+tIYVVXBMM##WWWW######MMWWMMMMWWWWWWWWWB
MMMMMMMMWWWWWWWWMMMMBBBYYIRBBXXBBBMI=====+iitIYVXRM###W#######MMWWMMMMWWWMMMMMMB
MMMMMMMMWWWWWWWWMMMBBBBXXVRBBRBRBRX==;;;===++tIYYXRBW#W##WWWW#MMWMMMMMWWWMWMMMMB
MMMMMMMMWWWWWWWWMMBBBBBBRVVXXRRRRRI=;;;;;==++itIYVRRMW#WWWMMWWMMWMMMMMMWWMMMMMMM
MMMMMMMWWWWWWWWWMMBBBBBBRRBVXIRXXVi;;:::;;=+iitIYVRBBW##MMMMMMBMWMMMMMMWWMMMMMMB
BBBBBBBBBBBBBBBBBBBBBBBBBRRBXBXVVY=;;:,:;;=++itIYVRBBM##WMWMMMBMWMMMMMMWWMMMMMMR
RRRRRRRRRRXXXXXXXXRBBBBBBRR#MXXRXI=;:,::;;=++itIYVRBBM###MWWWMBMWMMMMMMWWMMMMMMB
RRRRRRRBRRRRRRRRXXXBBBBBBBBWBRBRVi=;:,::;==++itIYXRBBM###WWWWMMMWMMMMMMWWMMMMMMB
BBBBBBBBBBBBBBRRXXXBBBBRRRMBBBRBV+=;::::==++iitIVXRBBM########MBWMBMMMMWMMMMMMMB
BMBBBBBBBBBBBBBRXVVRBRBRRRRRRBMMY==;::::==+++itYVXRBBM########WBWMBMMMMWMBMBMMBB
BBBMBBBBBBBBBBBRXVVRBRRRRRRRRBMMt==;::,:;+ii+itYVXRBBM########WBWMBBBBMWMBMBBMBR
BBBBBBBBBBBBBBBRVVVRBRRRRRRRRRMMi===ii+==+i++iIYVRRBBM######WWWBWMBBBBMWMBMBBMBR
BBBBBBBRBBBBRBBRVVVRRRRRRRRRRXYB+++i++tYtit+itYVXRBBBM########MBMMBBBBMWMBMBBMBR
BBBBBRBRBRBRRBRRVYVRRRRRRRRRRXYV+++=;+IIi==tXBMBBBBBMM#######WBBMMBBBBMWMBMBBMBR
BBBBBBRRBBBRRBRXYYVRRRRRRRRRXVBX+++++XXX=:;VRRiiXMMMMM########WBWMBBBBMWMRMBBMBR
RRRBBRRRRRRRRBRXYYVRRRRRRRRRXXBt+=;=+IVI::=XRWRVRBMBBM######MRRBWMRBBBMWMRMBBBRR
RRRRRRRXXRRRRRRXYYVRRRRRRRRRXVX++=;=+i=::;iRRMVYRBMBBM#W###WWBXBWMRBBBMWBRBBRBRR
RRRRRRXXXXRRXRXVIYVBRRRRBRRRRRV=+=;:+i=::;IRRRXXRBBBBMWM##WWBBBBWMRBRBMWBRBRRBRX
RRRRRXXXXXRXXXXVIIXBBBBBBBBBBXI++=::+i+;;=VRRXXXRRBBBWMM###MMBBMMBRRRBMWBXBRRBRX
RRRRXXXXXXXXXXXVIIXMMBBBBBBBBBY+++=+IX+;;+XRRRXXRRRBBMM#WWWBMBBBBBRRRRBWBXBRRBRX
RRRXXXXXXXXXXXVYtIXBBBBBBBBBBVY+++iYB+;;:tXRRBRXXRBBMWMWWWBBRRRRRRRRRRBWBXBRRBXX
RRRXXXVXXXXXXXVIttVRRRRRRRRRRRt+++tY+==iiIRRRBBXRBBMMMMWRXXXXRRRXXRRRRBWBXRXXRXV
XXXXXXVVXVVVXVYIttXRRRRRRRRRRVB+++itYItIYVMMRBBRRBBMMMMRXXXXRRRRRRRRRRBWBVRXXRVV
VVVVVVVVYYVVVVYtitVRRRRRRRRRRXR++++IVti+tIBMBRRRBBMMMMBRRRRRRRRRRRRXXXBWBVRXXRVV
YYVYYYYYYYVYYYYtiiVRRRRRRRRRRXYii+iIiY.:RXRRBBBBBBMMWBXXXXXXXXXXXXXXXXRWBVRXVRVV
VYYYVYYYYVVYIYYiiiVRRRRRRRRRXIVIItiI;+t;:;XMBBBBBBMMWWXXXXXXXXXXXXXXVXRWRYXVVRYV
VIYYVYYYYYYIIVIi++VRRRRRRRXXYVXVYVti;==ItIXBBRBBBMMWWWRXXXXXXXXXXXXVVVRWRIXVYXYY
YIYYYIIIYYYtIYI+++VXXXXXXXXXVXXRVYY+==tVVRBBBBBBMMW##XBXXXVVVVVVVVVVVVRWRIVYYXII
IIYVIIIIIIItttt+++YXXXXXXXXXXXIMIVY+;=+YXXRRBBMMWWW##MXVXVVVVVVVVVVYVVRWRIVYYXII
ttYYIIIIIIItttt=++YYYYYVVVVVVI+BtXVI==+tVVVXBBWWW#W##MMBVYYYYYYYYYYYYVXWRIVYIXIi
tiIItIIttIItit;+=+YYYYYYYVVViV=ItIRXtttYVVRRBWW##WW##MVYYYYYYYYYYYYYYYXWRtVYIXIi
iiItitiiitIii=:I;tVVVVVVVYIttR=+ItYRRXVXRRMMW###WWW###WVYYYYYVYYYYIYYYXWXtYYIXti
iiYtiiiiitI++:+VYiYVV    .;YWR+++IR     WMW#     #Y     IXXXXXXXRXtIYYXWXtYYIVti
iiItiiiitIt+:;IVI;IYV         I+i;       ###     Wi     IWXVVVVXMRtIYYXWXtYIIVtt
titii++iiii=,+IY;;tIi         ++R         M#     VV     I##WRYYVWXtIIYVWXtYItVtI
ttti++++iii::::t:+tYY    #X    i#    B    MW     .M     I###WMBVWXtIIYVWXtYItVtt
ttiiiiiiii+,,:=VIYVVY    #W    iV    V    VW      M     I####WWMMBIIIIVWXtYItVtt
ttiiiiiiii;,:+XVVVVYV    #W    iI    Y    XM      t     I####WWWWMMBRIVWXiIItVt+
++++++++it,,+XRXXXVVX    #W    it    B    XM   =        I###WWWWWWMMMBBBXiIItVi+
+=====tYIi,=YRRRXXVXX    #W    Yt    Y    RM   R        I###WWWWWMMMMMMMMBIItViY
VYYIIIYYY;;YVVXXXXXXR    #W    Mt    M    BW   R    I   IW##MWWWMMMMMMMWWWWtiYiV
XVVYYYVVY:;;,iXXXXXXX    #W    Mt    R    BW   R    W   IW#WMMMMMMBBMMMWWWWWMYiV
VYYYYVXXi,::+VXXRRRXY    #W    Xt    W    WW   R    W   IWMMMMMM:;+;;;MWWWB##YiW
#WWWVXRB::=IXXXXRRXVt    #W    RV    #    #M   R    X   IBMMMMMBV+RB:WMWWWM##RiW
WWWXVXRR;=YVRRXXXRXIY    #B    RM    #    #W   R.   X   IXMMMMBBV=..:WWWWBW###iW
WWWBXXRY=IXVRXXXVYIVV          BW    W    ##   RX   R   IVBWWMBRV=It:WWWMM####+W
WWWWMRR++VRXXXXXXXXXR         IWV        #W#   RW   X   IXR##BRXY=BW:WVMXVV###iW
WW#VBBR+=YXXXXXXXXRRR        ,MWYM      WWB#   RR  =X   IXB#WBRX++WR;BVYXXV###+W
WWRXRBR+=tVXXXXXRXXRRBBBBBRRRRM#YVWR###XMRWWWW#BtYYXVBBRRXB#BRRXVVVVVVYRRVX###iW
WMXRRBR+=iVXXXXXXXXRRRRRRRRRRRB#VV#WXBXBXMBWMWMMi=:;YVVVVXRMBBBV=+..:I;,iRI###+M
*/
  //return table.children[0].children[0].children;
  var tbody = table.children[0];
  var tr = tbody.children[0];
  var tds = tr.children;
  var ret = []
  //can't map() over these things, they are HTMLCollections not Arrays :)
  for (var i = 0; i < tds.length; i++) {
    ret.push(tds[i].children[0]);
  };
  return ret;
}

function findRadioValue(id) {
  var radios = getTableElements(document.getElementById(id));
  for (var i = 0; i < radios.length; i++) {
    if (radios[i].checked) {
      return radios[i].value;
    }
  }
}

// Select a random function from a list
function selectRandom(list) {
  return list[Math.floor(Math.random() * list.length)];
}

function computeNoteFrequencies() {
  var notes = [];
  var noteRatio = 1.05946309436 //2^(1/12)
  notes[0] = 4.35;
  for (var i = 1; i <= 288; i++) {
    notes[i] = notes[i - 1] * noteRatio;
  }
  return notes;
}


var tempo = 120.0;
var crotchet = 1 / (tempo / 60);
var eighth = crotchet / 2;

function addOctave(arr) {
  return arr.concat(arr,arr.map(function(elem) {return elem + 12}));
}

var arpeggioNotes;
var arpeggio;
var notes;

var chordTypes = {
  major: [0, 4, 7],
  minor: [0, 3, 7],
  augmented: [0, 4, 8],
  dominant_seventh: [0, 4, 7, 10]
}

function addChordTypeRadios() {
  console.log("adding chord types");
  var tr = document.getElementById("chordTypeTable").children[0].children[0]
  for (var key in chordTypes) {
    tr.innerHTML += '<td ><input type="radio" name="chordType" value = "'
      + key + '" checked="checked" />'
      + key + '</td>'
  }
}

function setArpeggio() {
  var baseNote = parseInt(findRadioValue("baseNoteTable")) + 70;
  var type = findRadioValue("chordTypeTable");
  arpeggioNotes = chordTypes[type].map(function(e) { return baseNote + e});
  arpeggio = arpeggioNotes.map(function(e) { return notes[e]});
}

function addNotes(time) {
  for (var i = 1; i <= 4; i++) {
    for (var j = 0; j < selectRandom([0, 1, 2, 3]); j++) {
      scheduler.scheduleObjects.push(new Ping(
                                          context,
                                          time + i * eighth,
                                          selectRandom(arpeggio)));
    }
  }
}

function start() {
  addChordTypeRadios();
  notes = computeNoteFrequencies();
  setArpeggio();
  console.log(arpeggio);
  document.getElementById("baseNoteTable").onchange = setArpeggio;
  document.getElementById("chordTypeTable").onchange = setArpeggio;
  scheduler = new MusicScheduler(context, addNotes);
  scheduler.start();
  // It's not clear whether onchange happens before or after the change is applied 8==D
  // This isn't a problem with onclic.
  document.getElementById("play").onclick = function(ev) {
    if (this.checked) {
      scheduler.start();
    } else {
      scheduler.stop();
    }
  }
}