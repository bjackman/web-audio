function ScheduleObject(context, time, scheduleNote) {
  this.context = context;
  this.time = time;
  this.scheduleNote = scheduleNote;
}

function MusicScheduler(context, exhausted) {
  var context = context;
  this.exhausted = exhausted;
  this.scheduleObjects = [];

  // When the audio time represented by the remaining schedule objects falls
  // below this value, the exhausted callback will be called.
  var lookAheadTime = 0.1;
  // The window of time for which each call to schedule() should schedule events
  // In seconds
  var scheduleWindowSize = 0.05;
  // The overlap time between scheduling windows
  // In seconds
  var scheduleWindowOverlap = 0.01;

  var playing = false;

  this.start = function() {
    if (!playing) {
      playing = true;
      // workaround to force the audio clock to start
      new Ping(context, context.currentTime, 0).scheduleNote();
      schedule.call(this, context.currentTime);
    }
  }
  this.stop = function() {
    this.scheduleObjects = []
    playing = false;
  }

  function schedule(lastEventTime) {
    var now = context.currentTime;
    var nextObject = this.scheduleObjects[0];
    // Put objects in the scheduling window into the audio queue
    while (nextObject && nextObject.time <= now + scheduleWindowSize) {
      nextObject.scheduleNote();
      lastEventTime = nextObject.time;
      this.scheduleObjects.shift();
      nextObject = this.scheduleObjects[0];
    }
    // call the exhausted callback if we need to
    var lastObject = this.scheduleObjects[this.scheduleObjects.length -1];
    if (this.scheduleObjects.length == 0) {
      this.exhausted(lastEventTime);
    } else if (lastObject.time - now < lookAheadTime) {
      this.exhausted(lastObject.time);
    }
    if (playing) {
      var outerThis = this; //this inside the callback is "global" :(
      window.setTimeout(function() {
        schedule.call(outerThis, lastEventTime);
        }, (scheduleWindowSize - scheduleWindowOverlap) * 1000 );
    }
  }
}
