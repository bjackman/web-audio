function setColour(shape, colour) {
  shape.attr("fillColor", colour);
  shape.stroke(colour, 1);
}
function getFillColour(shape) {
  return color(shape.attr("fillColor"));
}

var squareSize = 20;
var squares = [];
var squaresContainer = new Group().addTo(stage);

var lastColour = color("red");
for (var x = 0; x < (stage.width - squareSize); x += squareSize) {
  squares[x] = [];
  for (var y = 0; y < (stage.height - squareSize); y += squareSize) {
    thisColour = 
    squares[x][y] = new Rect(y, x, squareSize, squareSize)
      .attr("fillColor", lastColour)
      //.stroke(lastColour, 1)
      .addTo(squaresContainer)
      .addTo(stage);
    lastColour = lastColour.hue((lastColour.hue() + 0.05) % 1);
  }
}

console.log(squares[0][0]);

function fade(square) {
  var oldColour = getFillColour(square);
  var newColour = oldColour.hue(oldColour.hue() + 0.05);
  square.animate("400ms", {
    strokeColor : newColour,
    fillColor : newColour
  }, {
    easing : "sineInOut",
    onEnd : function() { fade(square); }
  });
}

squares.forEach(function(arr, i, squares) {
  arr.forEach(function(square, j, row) {
    fade(square);
  });
});
/*
var frequencyProportion = 0;
stage.on("message:change", function(data) {
  var freqDelta = data.frequencyProportion - frequencyProportion;
  frequencyProportion = data.frequencyProportion;
  squares.forEach(function(arr, i, squares) {
    arr.forEach(function(square, j, row) {
      var colour = getFillColour(square);
      var newHue = (colour.hue() + freqDelta);// % 1;
      console.log(newHue);
      setColour(square, colour.hue(newHue));
    });
  });
});
*/
