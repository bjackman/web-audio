
function schedulePing() {
  var osc = this.context.createOscillator();
  var gainNode = this.context.createGain();

  osc.connect(gainNode);
  gainNode.connect(this.context.destination);

  osc.frequency.value = this.frequency;

  osc.noteOn(this.time);
  //attack
  gainNode.gain.linearRampToValueAtTime(0, this.time);
  gainNode.gain.linearRampToValueAtTime(0.4, this.time + this.attack);
  //decay
  gainNode.gain.exponentialRampToValueAtTime(
                  0.000001,
                  this.time + this.attack + this.decay);
  //release
  osc.noteOff(this.time + (this.attack) + (this.decay) + (this.release));
}

function Ping(context, time, frequency, attack, decay, release) {
  // inherit from ScheduleObject:
  ScheduleObject.call(this, context, time, schedulePing);
  this.frequency = frequency;
  this.attack = attack || 0.001;
  this.decay = decay || 2.0;
  this.release = release || 0.1;
}
